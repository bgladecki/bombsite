class RenamePostIdIdToPostId < ActiveRecord::Migration
  def change
    rename_column :videos, :post_id_id, :post_id
  end
end
