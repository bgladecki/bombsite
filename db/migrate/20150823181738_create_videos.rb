class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :link
      t.references :post, index: true, foreign_key: true
    end
  end
end
