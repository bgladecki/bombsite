class AddMemeColumnsToPost < ActiveRecord::Migration
  def up
    add_attachment :posts, :meme
  end

  def down
    remove_attachment :posts, :meme
  end
end
