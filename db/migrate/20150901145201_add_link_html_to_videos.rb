class AddLinkHtmlToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :link_html, :text
  end
end
