class AddPostIdToVideoAndRemoveVideoIdFromPostInOrderToMakeShitWorkTheRightWayThisTime < ActiveRecord::Migration
  def change
    remove_column :posts, :video_id
    add_reference :videos, :post_id, index: true, foreign_key: true
  end
end
