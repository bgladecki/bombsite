class AddVideoIdToPostAndRemovePostIdFromVideo < ActiveRecord::Migration
  def change
    add_column :posts, :video_id, :integer
    remove_column :videos, :post_id, :integer
  end
end
