Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/quickwizard', as: 'rails_admin'
  resources :posts, except: :edit do
    resources :comments, only: [:create, :destroy]
    get 'my', on: :collection
    member do
      put 'like'
      put 'dislike'
    end
  end
  root to: 'posts#index'
  devise_for :users
  resources :users
end
