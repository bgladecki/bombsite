class PostPolicy #< ApplicationPolicy
  attr_reader :current_user, :post

  def initialize(current_user,post)
    @current_user = current_user
    @post = post
  end

  def index?
    true
  end

  def show?
    true
  end

  def new?
    @current_user
  end

  def my?
    new?
  end

  def create?
    new?
  end

  def destroy?
    return false unless @current_user
    (@post.user == @current_user) || (@current_user.admin?) || (@current_user.moderator?) 
  end

  def like?
    current_user
  end

  def dislike?
    current_user
  end
end
