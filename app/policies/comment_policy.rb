class CommentPolicy #< ApplicationPolicy
  attr_reader :current_user

  def initialize(current_user, comment)
    @current_user = current_user
    @comment = comment
  end

  def edit?
    (@comment.user == @current_user) || (@current_user.admin?) || (@current_user.moderator?)
  end

  def create?
    @current_user
  end

  def update?
    edit?
  end

  def destroy?
    edit?
  end
end
