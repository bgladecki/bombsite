class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user!, except: [:index, :show]
  after_action :verify_authorized, except: [:index, :show]

  def index
    @posts = Post.includes(:video).page(params[:page]).per(10)
  end

  def my
    @posts = current_user.posts.includes(:video).page(params[:page]).per(15)
    authorize Post

    if @posts.any?
      render 'index'
    else
      render 'no_posts_for_this_user'
    end
  end

  def show
    @post = Post.includes(:video, :comments).find(params[:id])
    if current_user
      begin
        authorize @post, :destroy?
      rescue Pundit::NotAuthorizedError
        @am_i_even_allowed = false
      end

      @am_i_even_allowed = true unless defined? @am_i_even_allowed
    end
  end

  def new
    @post = Post.new
    @post.build_video
    authorize @post
  end

  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id
    authorize @post
    if post_params[:meme].nil?
      @post.build_video(post_params[:video_attributes])
    else
      @post.video.delete unless @post.video.nil?
    end

    if @post.save
      redirect_to @post
    else
      redirect_to :back, alert: 'Post was NOT added.'
    end
  end

  def destroy
    authorize @post
    @post.destroy
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  def like
    set_post
    authorize @post
    render 'like_dislike' if @post.upvote_by current_user
  end

  def dislike
    set_post
    authorize @post
    render 'like_dislike' if @post.downvote_by current_user
  end

  private


  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def post_params
    params.require(:post).permit(:title, :meme, video_attributes: [:id, :link])
  end
end
