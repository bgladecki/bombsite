class Post < ActiveRecord::Base
  default_scope -> { order('created_at DESC') }
  validates_presence_of :title
  belongs_to :user
  has_one :video, dependent: :destroy
  accepts_nested_attributes_for :video
  has_attached_file :meme, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :meme, :content_type => /\Aimage\/.*\Z/
  acts_as_commentable
  acts_as_votable

  def user_owns_it?
    user == current_user
  end

end
