class Video < ActiveRecord::Base
  belongs_to :post
  validates_format_of :link, with: /(?:http\:\/\/)?(?:www\.)?(youtube|vimeo|dailymotion)\.(com|pl)/

  auto_html_for :link do
    html_escape
    image
    youtube(:width => 640, :height => 385, :autoplay => false)
    link :target => "_blank", :rel => "nofollow"
    simple_format
  end

end
